<?php

require '../vendor/autoload.php';

use Symfony\Component\Finder\Finder;

$files = Finder::create()
    ->in('/home/kusal/Code/Laracasts/Discover Symfony Components/finder-episode')
    ->name('*.txt')
    ->contains('{example-key}');

foreach ($files as $file) {
    $contents = $file->getContents();

    $updated = str_replace('{example-key}', 'UPDATED', $contents);

    file_put_contents($file->getRealPath(), $updated);
}
