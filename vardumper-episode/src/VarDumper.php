<?php

require '../vendor/autoload.php';

class Tasks
{
    protected $tasks;

    public function __construct($tasks)
    {
        $this->tasks = $tasks;
    }
}

class Task
{
    public $description;

    public function __construct($description)
    {
        $this->description = $description;
    }
}

$tasks = new Tasks([
    new Task('Go to the store'),
    new Task('Finish this screencast'),
    new Task('Read something')
]);

dump($tasks);
